package com.redshepherd.RedPay;

// import javax.crypto.Cipher;
// import javax.crypto.NoSuchPaddingException;
// import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.codec.binary.Base64;

import java.security.SecureRandom;

public class Session {
  private String app;
  private String key;
  private String endpoint;

  private Session(String app, String key, String endpoint) {
    this.app = app;
    this.key = key;
    this.endpoint = endpoint;

    // Step 1: Generate random IV
    byte[] iv = new byte[16];
    SecureRandom prng = new SecureRandom();
    prng.nextBytes(iv);
    String base64Iv = Base64.encodeBase64String(iv);

    // // Generate and save Random key
    // try {
    // // In Java, standard padding is called PKCS5Padding although it actually
    // // performs PKCS #7 padding
    // Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
    // // cipher.init(Cipher.ENCRYPT_MODE, this.key, new IvParameterSpec(iv));

    // } catch (NoSuchPaddingException nsp_ex) {
    // }
  }
}
